package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class BalaesabTest {
    @Test
    public void factorialTest() {
        Balaesab balaesab = new Balaesab();
        int n = 2;
        long expectedResult = 2;

        long result = balaesab.factorial(n);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void factorialTest2() {
        Balaesab balaesab = new Balaesab();
        int n = 1;
        long expectedResult = 1;

        long result = balaesab.factorial(n);

        Assertions.assertEquals(expectedResult, result);
    }
}