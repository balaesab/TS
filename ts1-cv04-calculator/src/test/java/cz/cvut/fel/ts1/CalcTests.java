package cz.cvut.fel.ts1;

import org.junit.jupiter.api.*;

public class CalcTests {
    Calc calc;


    @BeforeEach
    public void setup() {
        calc = new Calc();
    }

    @Test
    public void Add_TwoPlusFour_Six() {
        int a = 2;
        int b = 4;
        int expectedResult = 6;

        int result = calc.add(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Divide_TwoDividedByZero_ExceptionThrow() {
        int a = 2;
        int b = 0;

        Assertions.assertThrows(Exception.class, () -> calc.divide(a, b)); //lambda fce
    }

    @Test
    public void Divide_TwoDividedByOne_Two() {
        int a = 2;
        int b = 1;
        int expectedResult = 2;

        int result = calc.divide(a, b);

        Assertions.assertEquals(expectedResult, result); //lambda fce
    }

    @Test
    public void Subtract_TwoMinusOne_One() {
        int a = 2;
        int b = 1;
        int expectedResult = 1;

        int result = calc.subtract(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @Test
    public void Multiply_TwoTimesOne_Two() {
        int a = 2;
        int b = 1;
        int expectedResult = 2;

        int result = calc.multiply(a, b);

        Assertions.assertEquals(expectedResult, result);
    }

    @AfterEach
    public void closeEach() {

    }

}
